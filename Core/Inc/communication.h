/**
 * @file communication.h
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Header file for comunication with PC module
 * @version 1.0
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#define PC_FRAME_LENGTH 2048

typedef struct {
	uint16_t distance;
	uint8_t angle;
} LIDAR_Data;

typedef struct {
	char identifier[2];
	int16_t speed_left_motor;
	int16_t speed_right_motor;
	int16_t acceleration_left_motor;
	int16_t acceleration_right_motor;
	uint16_t x_axis_joystick;
	uint16_t y_axis_joystick;
	uint16_t battery_voltage;
	LIDAR_Data lidar_table[200];
	uint8_t last_lidar_index;
	uint16_t crc16;
} PC_FrameData;

/**
 * @brief Initialize communication with PC computer
 * @details Set up identifier and set defaults values
 * @param[in] frame_data Handle to structure data of frame
 * @return Status of setting values
 */
uint8_t PCCOM_Init(PC_FrameData *frame_data);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[in] left_motor Speed value of left motor
 * @param[in] right_motor Speed value of right motor
 * @return Status of setting values
 */
uint8_t PCCOM_SetMotorsSpeed(PC_FrameData *frame_data, int16_t left_motor,
		int16_t right_motor);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[in] left_motor Acceleration value of left motor
 * @param[in] right_motor Acceleration value of right motor
 * @return Status of setting values
 */
uint8_t PCCOM_SetMotorsAcceleration(PC_FrameData *frame_data,
		int16_t left_motor, int16_t right_motor);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[in] x_axis Value of X axis swing
 * @param[in] y_axis Value of Y axis swing
 * @return Status of setting values
 */
uint8_t PCCOM_SetJoystickSwing(PC_FrameData *frame_data, uint16_t x_axis,
		uint16_t y_axis);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[in] battery_voltage Battery voltage value
 * @return Status of setting values
 */
uint8_t PCCOM_SetBatteryVoltage(PC_FrameData *frame_data,
		uint16_t battery_volatage);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[in] lidar_data Structure containing lidar data [angle and distance]
 * @return Status of setting values
 */
uint8_t PCCOM_AddOneLidarMeasurement(PC_FrameData *frame_data,
		LIDAR_Data lidar_data);

/**
 * @brief
 * @param[in] frame_data Handle to structure data of frame
 * @param[out] buffer Buffer to save one frame which will be sent to PC
 * @return Status of saving data to buffer
 */
uint8_t PCCOM_ConstructFrame(PC_FrameData *frame_data,
		char buffer[PC_FRAME_LENGTH]);

#endif /* COMMUNICATION_H_ */
